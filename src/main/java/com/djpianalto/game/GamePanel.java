package com.djpianalto.game;

import com.djpianalto.game.graphics.Font;
import com.djpianalto.game.graphics.Sprite;
import com.djpianalto.game.states.PlayState;
import com.djpianalto.game.utils.KeyHandler;
import com.djpianalto.game.utils.MouseHandler;
import com.djpianalto.game.utils.Position;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class GamePanel extends JPanel implements Runnable {

    public static int width;
    public static int height;
    public static int generation = 0;
    public static int oldFrameCount;
    public static boolean stable = false;

    private int columns = 0;
    private int rows = 0;

    private Thread thread;
    private boolean running = false;
    private boolean autoRun = false;
    private boolean manualStep = false;
    private BufferedImage img;
    private Graphics2D g;
    private KeyHandler key;
    private MouseHandler mouse;

    private PlayState playState;


    public GamePanel(int width, int height) {
        this.width = width;
        this.height = height;
        setPreferredSize(new Dimension(width, height));
        setFocusable(true);
        requestFocus();
    }

    public void addNotify() {
        super.addNotify();
        if (thread == null) {
            thread = new Thread(this, "GameThread");
            thread.start();
        }
    }

    private void init() {
        this.running = true;
        img = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_ARGB);
        g = (Graphics2D) img.getGraphics();

        key = new KeyHandler(this);
        mouse = new MouseHandler(this);

        selectSize();
        createBoard();
    }

    private void createBoard() {
        generation = 0;

        playState = new PlayState(columns, rows, this);
        stable = false;

        render();
        draw();
    }

    public void run() {
        init();

        final double GAME_REFRESH_RATE = 100.0;
        final double TIME_BEFORE_UPDATE = 1000000000 / GAME_REFRESH_RATE;

        final int UPDATES_BEFORE_RENDER = 1;

        double lastUpdateTime = System.nanoTime();
        double lastRenderTime;

        final double TARGET_FPS = 60;
        final double TIME_BEFORE_RENDER = 1000000000 / TARGET_FPS;

        int frameCount = 0;
        int lastSecondTime = (int) (lastUpdateTime / 1000000000);

        while (running) {

            input(key);

            if (manualStep && !autoRun) {
                update();
                generation++;
                render();
                draw();
                manualStep = false;
            }

            while (autoRun) {
                input(key);

                double now = System.nanoTime();

                if (now - lastUpdateTime > TIME_BEFORE_UPDATE) {
                    update();
                    generation++;
                    render();
                    draw();

                    frameCount++;

                    lastUpdateTime = now;
                }

                int thisSecond = (int) (lastUpdateTime / 1000000000);
                if (thisSecond > lastSecondTime) {
                    if (frameCount != oldFrameCount) {
                        oldFrameCount = frameCount;
                    }
                    frameCount = 0;
                    lastSecondTime = thisSecond;
                }

                Thread.yield();

                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    System.out.println("ERROR: yielding thread");
                }
                now = System.nanoTime();
            }
            Thread.yield();

            try {
                Thread.sleep(1);
            } catch (Exception e) {
                System.out.println("ERROR: yielding thread");
            }
        }
    }

    private void input(KeyHandler key) {
        if (key.space.clicked) {
            manualStep = true;
            autoRun = false;
            key.space.clicked = false;
        } else if (key.enter.clicked) {
            if (autoRun) {
                autoRun = false;
            } else {
                autoRun = true;
            }
            manualStep = false;
            key.enter.clicked = false;
        } else if (key.restart.clicked) {
            if (!autoRun) {
                restart();
            }
            key.restart.clicked = false;
        }
    }

    private void restart() {
        rows = 0;
        columns = 0;
        autoRun = false;
        selectSize();
        createBoard();
    }

    private void update() {
        try {
            playState.update();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        manualStep = false;
        autoRun = false;
        stable = true;
    }

    private void selectSize() {
        if (g != null) {
            g.setColor(new Color(66, 134, 244));
            Font font = new Font("fonts/font.png", 10, 10);
            g.fillRect(0,0, this.width, this.height);
            int startingY = 200;
            int startingX = 200;
            int fontSize = 40;
            Sprite.drawArray(g, font, "How many Rows and Columns?", new Position(startingX, startingY), fontSize - 10, fontSize - 10, fontSize - 20, 0);
            Sprite.drawArray(g, font, "  10 x  10", new Position(startingX + 100, startingY + 10 + fontSize), fontSize, fontSize, fontSize - 10, 0);
            Sprite.drawArray(g, font, "  50 x  50", new Position(startingX + 100, startingY + (10 + fontSize) * 2), fontSize, fontSize, fontSize - 10, 0);
            Sprite.drawArray(g, font, " 100 x 100", new Position(startingX + 100, startingY + (10 + fontSize) * 3), fontSize, fontSize, fontSize - 10, 0);
            Sprite.drawArray(g, font, " 200 x 200", new Position(startingX + 100, startingY + (10 + fontSize) * 4), fontSize, fontSize, fontSize - 10, 0);
            Sprite.drawArray(g, font, " 500 x 500", new Position(startingX + 100, startingY + (10 + fontSize) * 5), fontSize, fontSize, fontSize - 10, 0);
        }
        draw();
        while(rows == 0) {
            waitForMouseClick();

            Thread.yield();

            try {
                Thread.sleep(1);
            } catch (Exception e) {
                System.out.println("ERROR: yielding thread");
            }
        }
    }

    private void waitForMouseClick() {
        if (mouse.getMouseB() == 1) {
            if (240 < mouse.getMouseY() && mouse.getMouseY() < 280) {
                rows = 10;
                columns = 10;
            } else if (290 < mouse.getMouseY() && mouse.getMouseY() < 330) {
                rows = 50;
                columns = 50;
            } else if (340 < mouse.getMouseY() && mouse.getMouseY() < 380) {
                rows = 100;
                columns = 100;
            } else if (390 < mouse.getMouseY() && mouse.getMouseY() < 430) {
                rows = 200;
                columns = 200;
            } else if (440 < mouse.getMouseY() && mouse.getMouseY() < 480) {
                rows = 500;
                columns = 500;
            }
        }
    }

    private void render() {
        if (g != null) {
            g.setColor(new Color(66, 134, 244));
            g.fillRect(0,0, this.width, this.height);
            playState.render(g);
        }
    }

    private void draw() {
        Graphics g2 = (Graphics) this.getGraphics();
        g2.drawImage(img, 0, 0, this.width, this.height, null);
        g2.dispose();
    }
}
