package com.djpianalto.game.utils;

public class Position {

    private float x;
    private float y;

    private static float worldX;
    private static float worldY;

    public Position() {
        this.x = 0;
        this.y = 0;
    }

    public Position(Position position) {
        new Position(position.x, position.y);
    }

    public Position(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void addX(float i) {
        this.x += i;
    }

    public void addY(float i) {
        this.y += i;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public void setVector(Position position) {
        this.x = position.x;
        this.y = position.y;
    }

    public void setVector(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public static void setWorldVar(float x, float y) {
        worldX = x;
        worldY = y;
    }

    public Position getWorldVar() {
        return new Position(this.x - worldX, y - worldY);
    }

    @Override
    public String toString() {
        return this.x + ", " + this.y;
    }

}
