package com.djpianalto.game.utils;

import com.djpianalto.game.GamePanel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class KeyHandler implements KeyListener {

    public static List<Key> keys = new ArrayList<Key>();

    public class Key {
        public int presses, absorbs;
        public boolean down, clicked;

        public Key() {
            keys.add(this);
        }

        public void toggle(boolean pressed) {
            if (pressed != down) {
                down = pressed;
            }
            if (pressed) {
                presses++;
            }
        }

        public void tick(boolean click) {
            clicked = click;
        }
    }

    public Key enter = new Key();
    public Key space = new Key();
    public Key restart = new Key();
    Set<Integer> pressedKeys = new TreeSet<Integer>();

    public KeyHandler(GamePanel game) {
        game.addKeyListener(this);
    }

    public void releaseAll() {
        for (Key key : keys) {
            key.down = false;
            key.clicked = false;
        }
    }

//    public void tick() {
//        for (Key key : keys) {
//            key.tick();
//        }
//    }

    public void toggle(KeyEvent keyEvent, boolean pressed) {

    }

    public void keyTyped(KeyEvent keyEvent) {
        // Do Nothing... ;)
    }

    public void keyPressed(KeyEvent keyEvent) {
        int code = keyEvent.getKeyCode();
        Integer val = Integer.valueOf(code);
        if (pressedKeys.contains(val)) {
            return;
        } else {
            pressedKeys.add(val);

            if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                enter.tick(true);
            }
            if (keyEvent.getKeyCode() == KeyEvent.VK_SPACE) {
                space.tick(true);
            }
            if (keyEvent.getKeyCode() == KeyEvent.VK_R) {
                restart.tick(true);
            }
        }
    }

    public void keyReleased(KeyEvent keyEvent) {
        pressedKeys.remove(keyEvent.getKeyCode());
    }
}
