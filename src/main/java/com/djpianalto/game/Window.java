package com.djpianalto.game;

import javax.swing.JFrame;

public class Window extends JFrame {

    public Window() {
        setTitle("Fun Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setContentPane(new GamePanel(980, 980));

        pack();
        setLocationRelativeTo(null);

        setVisible(true);
    }

}
