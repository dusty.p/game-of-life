package com.djpianalto.game.states;

import java.awt.*;

public abstract class GameState {

    private PlayState playState;

    public GameState(PlayState playState) {
        this.playState = playState;
    }

    public abstract void update();
    public abstract void render(Graphics2D g);
}
