package com.djpianalto.game.states;

import com.djpianalto.game.GamePanel;
import com.djpianalto.game.graphics.Font;
import com.djpianalto.game.graphics.Sprite;
import com.djpianalto.game.utils.Position;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class PlayState {

    private int[][] gameBoard;
    private int[][] oldBoard1;
    private int[][] oldBoard2;

    private int columns = 100;
    private int rows = 100;
    private int cellSize;

    private final int OVERCROWDING = 4;
    private final int LONELINESS = 1;
    private final int BIRTH = 3;

    private Sprite sprite;
    private Font font;
    private GamePanel gamePanel;

    public PlayState(int columns, int rows, GamePanel gamePanel) {
        super();
        this.columns = columns;
        this.rows = rows;
        this.cellSize = ((GamePanel.width - 80) / columns);

        this.gamePanel = gamePanel;

        String imgPath;
        if (this.cellSize > 30) {
            imgPath = "cells.png";
        } else {
            imgPath = "cells_no_border.png";
        }

        this.sprite = new Sprite(imgPath, 150, 150);
        this.font = new Font("fonts/font.png", 10, 10);

        gameBoard = new int[columns][rows];
        Random generator = new Random();

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                gameBoard[i][j] = generator.nextBoolean() ? 1 : 0;
            }
        }

    }

    public void update() throws InterruptedException {
        final int[][] newGameBoard = deepCopyIntMatrix(gameBoard);
        oldBoard2 = deepCopyIntMatrix(oldBoard1);
        oldBoard1 = deepCopyIntMatrix(gameBoard);

        int colsPerThread = 10;
        int threadCount = columns / colsPerThread;

        Object[] threads = new Object[threadCount];

        int currentThread = 0;
        for (int i = 0; i < columns; i += colsPerThread) {
            int p = i;
            threads[currentThread] = new Thread(() -> updateColumn(p, colsPerThread, newGameBoard));
            currentThread++;
        }
        for (Object t : threads) {
            ((Thread) t).start();
        }
        for (Object t : threads) {
            ((Thread) t).join();
        }
        gameBoard = newGameBoard;
        if (Arrays.deepEquals(gameBoard, oldBoard1) || Arrays.deepEquals(gameBoard, oldBoard2)) {
            gamePanel.stop();
        }
    }

    private void updateColumn(int start, int colsPerThread, int[][] newGameBoard) {
        for (int i = start; i < start + colsPerThread; i++) {
            if (i >= columns) break;
            for (int j = 0; j < rows; j++) {
                int aliveNeighborCount = checkNeighbors(i, j, gameBoard);
                if (gameBoard[i][j] == 1) {
                    newGameBoard[i][j] = (aliveNeighborCount > LONELINESS && aliveNeighborCount < OVERCROWDING) ? 1 : 0;
                } else {
                    newGameBoard[i][j] = aliveNeighborCount == BIRTH ? 1 : 0;
                }
            }
        }
    }

    private int[][] deepCopyIntMatrix(int[][] input) {
        if (input == null)
            return null;
        int[][] result = new int[input.length][];
        for (int r = 0; r < input.length; r++) {
            result[r] = input[r].clone();
        }
        return result;
    }

    private int checkNeighbors(int i, int j, int[][] oldGameBoard) {
        int neighbors[][] = {
                {i - 1, j - 1},
                {i, j - 1},
                {i + 1, j - 1},
                {i - 1, j},
                {i + 1, j},
                {i - 1, j + 1},
                {i, j + 1},
                {i + 1, j + 1},
        };

        int aliveNeighborCount = 0;
        for (int[] neighbor : neighbors) {
            if (neighbor[0] == -1) { neighbor[0] = columns - 1; }
            else if (neighbor[0] == columns) { neighbor[0] = 0; }

            if (neighbor[1] == -1) { neighbor[1] = rows - 1; }
            else if (neighbor[1] == rows) { neighbor[1] = 0; }

            if (oldGameBoard[neighbor[0]][neighbor[1]] == 1) {
                aliveNeighborCount++;
            }
        }
        return aliveNeighborCount;
    }

    public void render(Graphics2D g) {

        int width = this.cellSize * columns;
        int height = this.cellSize * rows;

        g.setColor(Color.BLACK);
        g.drawRect(40, 40, width, height);

        for (int j = 0; j < rows; j++) {
            ArrayList<BufferedImage> images = new ArrayList<BufferedImage>();
            for (int i = 0; i < columns; i++) {
                images.add(sprite.getSprite(gameBoard[j][i], 0));
            }

            Sprite.drawArray(g, images, new Position(40, j * this.cellSize + 40), this.cellSize, this.cellSize, this.cellSize, 0);
        }

        Sprite.drawArray(g, font, "Generation " + GamePanel.generation + " FPS " + GamePanel.oldFrameCount, new Position(10, 10), 20, 20, 15, 0);
        Sprite.drawArray(g, font, "ENTER: play/pause  SPACE: Single Step  R: restart (when stopped)", new Position(10, GamePanel.height - 30), 20, 20, 15, 0);
        if (GamePanel.stable) {
            Sprite.drawArray(g, font, "Reached Stable State", new Position(GamePanel.width - 420, 10), 20, 20, 15, 0);
        }

    }

}
