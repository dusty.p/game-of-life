package com.djpianalto.game.graphics;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class Font {

    private final BufferedImage FONTSHEET;
    private BufferedImage[][] fontArray;
    private final int TILE_SIZE = 32;
    private int w;
    private int h;
    private int wLetter;
    private int hLetter;

    public Font(String file) {
        w = TILE_SIZE;
        h = TILE_SIZE;

        System.out.println("Loading: " + file + "...");
        FONTSHEET = loadFont(file);

        wLetter = FONTSHEET.getWidth() / w;
        hLetter = FONTSHEET.getHeight() / h;
        loadFontArray();
    }

    public Font(String file, int w, int h) {
        this.w = w;
        this.h = h;

        System.out.println("Loading: " + file + "...");
        FONTSHEET = loadFont(file);

        wLetter = FONTSHEET.getWidth() / w;
        hLetter = FONTSHEET.getHeight() / h;
        loadFontArray();
    }

    public void setSize(int width, int height) {
        setWidth(width);
        setHeight(height);
    }

    public void setWidth(int width) {
        this.w = width;
        this.wLetter = FONTSHEET.getWidth() / w;
    }

    public void setHeight(int height) {
        this.h = height;
        this.hLetter = FONTSHEET.getHeight() / h;
    }

    public int getWidth() {
        return this.w;
    }

    public int getHeight() {
        return this.h;
    }

    private BufferedImage loadFont(String file) {
        BufferedImage font = null;
        try {
            font = ImageIO.read(getClass().getClassLoader().getResourceAsStream(file));
        } catch (Exception e) {
            System.out.println("ERROR: could not load file: " + file);
        }

        return font;
    }

    private void loadFontArray() {
        fontArray = new BufferedImage[wLetter][hLetter];

        for (int x = 0; x < wLetter; x++) {
            for (int y = 0; y < hLetter; y++) {
                fontArray[x][y] = getLetter(x, y);
            }
        }
    }

    public BufferedImage getFontSheet() {
        return FONTSHEET;
    }

    public BufferedImage getLetter(int x, int y) {
        return FONTSHEET.getSubimage(x * this.w, y * this.h, this.w, this.h);
    }

    public BufferedImage getFont(char letter) {
        int value = letter;

        int x = value % wLetter;
        int y = value / wLetter;

        return getLetter(x, y);
    }

    public BufferedImage[] getFontArray(int i) {
        return fontArray[i];
    }

    public BufferedImage[][] getFontArray() {
        return fontArray;
    }

}
